# Loan Repayment:
spring boot project to generate repayment schedule

# Plan Generator:
In order to inform borrowers of the Cool Bank about the final repayment schedule, we need to have pre calculated repayment plans throughout the
lifetime of a loan.
To be able to calculate a repayment plan
plan, specific input parameters are necessary:
•duration (number of instalments in months)
•nominal rate (annual interest rate)
•loan amount (principal amount)
•date of disbursement/ payout ("startDate")
These four parameters need to be input parameters.
The goal is to calculate a repayment plan for an annuity loan. Therefore the amount that the borrower has to pay back every month, consisting of principal
and interest repayments, does not change (the last instalment might be an exception).
The annuity amount has to be derived from three of the input parameters (duration, nominal interest rate, total loan amount)
before starting the plan calculation.
(use http://financeformulas.net/Annuity_Payment_Formula.html as reference)


# Project Requirements:
Implement a web service that has one endpoint to generate a borrower plan via HTTP in JSON. Feel free to use any web service
you are most
comfortable with.

PORT: 8080
HTTP Method: POST
Media Type: application/JSON
Endpoint: http://localhost:8080/generate-plan

# Sample Request:
Request Payload:
{
"loanAmount": "5000",
"nominalRate": "5.0",
"duration": 24,
"startDate": "01-01-2024"
}


Response:
{
"borrowerPayments":[
{
"borrowerPaymentAmount":"219.36",
"date":"01-01-2024",
"initialOutstandingPrincipal":"5000.00",
"interest":"20.83",
"principal":"198.53",
"remainingOutstandingPrincipal":"4801.47"
},
{
"borrowerPaymentAmount":"219.36",
"date":"01-02-2024",
"initialOutstandingPrincipal":"4801.47",
"interest":"20.01",
"principal":"199.35",
"remainingOutstandingPrincipal":"4602.12"
},
...
{
"borrowerPaymentAmount":"219.28",
"date":"01-12-2025",
"initialOutstandingPrincipal":"218.37",
"interest":"0.91",
"principal":"218.37",
"remainingOutstandingPrincipal":"0"
}
]
}

# Assumptions:
The data type is assumed to be in decimal . So double value has been used for most of the input and outputs parameters.

# Closing Remarks:
Please design and implement the task in Java 8 or newe
newer. The code should be production ready ready, including tests. Along with the implementation implementation, please provide a short description for other developers on how to integrate and use this tool. Describe any shortcuts or assumptions your solution contains.
Please compress your solution and upload to a cloud provider of your choice (e.g. Google Drive or OneDrive), make it publicly accessible via a link and share it with the recruiter that contacted you.

