package com.payments.advice;

import java.util.logging.Logger;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.payments.exception.ErrorResponse;
import com.payments.exception.ExceptionConstants;

public class GlobalExceptionHandler {

	private static final Logger log = Logger.getLogger(GlobalExceptionHandler.class.getName());

	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ErrorResponse genericException(RuntimeException re) {
		log.severe(re.getMessage());
		ErrorResponse errorResponse = new ErrorResponse(ExceptionConstants.SERVER_ERROR + " : " + re.getMessage(),
				HttpStatus.INTERNAL_SERVER_ERROR);
		return errorResponse;

	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ErrorResponse handleValidationExceptions(MethodArgumentNotValidException ex) {
		log.severe(ex.getMessage());
		ErrorResponse errorResponse = new ErrorResponse(ExceptionConstants.INVALID_INPUTS + " : " + ex.getMessage(),
				HttpStatus.BAD_REQUEST);
		return errorResponse;

	}
}
