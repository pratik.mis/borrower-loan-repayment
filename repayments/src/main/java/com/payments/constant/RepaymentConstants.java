package com.payments.constant;

public interface RepaymentConstants {

	int NUMBER_OF_DAYS_IN_MONTH = 30;

	int NUMBER_OF_DAYS_IN_YEAR = 360;

	int NUMBER_OF_MONTHS_IN_YEAR = 12;

	int ROUND_OFF_PLACE = 2;
}
