package com.payments.constant;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class RoundOfNumber {
	
	public static double roundToNDecimalPlace(double value, int scale)
	{
		return new BigDecimal(value).setScale(scale, RoundingMode.HALF_UP).doubleValue();
	}

}
