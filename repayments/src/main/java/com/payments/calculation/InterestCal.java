package com.payments.calculation;

import org.springframework.stereotype.Component;

import com.payments.constant.RepaymentConstants;

@Component
public class InterestCal {

	public double calculateInterestPerPeriod(double initialPrinicipal, double nominalInterstRate) {
		double InterestAmount = (initialPrinicipal * RepaymentConstants.NUMBER_OF_DAYS_IN_MONTH * nominalInterstRate)
				/ (RepaymentConstants.NUMBER_OF_DAYS_IN_YEAR * 100);
		return InterestAmount;
	}

}
