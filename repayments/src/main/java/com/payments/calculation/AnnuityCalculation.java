package com.payments.calculation;

import org.springframework.stereotype.Component;

@Component
public class AnnuityCalculation {

	public double calculateAnnuity(double presetval, double interPerPeriod, int numOfPeriods) {

		double annuity = 0d;
		interPerPeriod = (interPerPeriod / 100);
		annuity = (interPerPeriod * presetval) / (1 - Math.pow((1 + interPerPeriod), -numOfPeriods));

		return annuity;
	}

}
